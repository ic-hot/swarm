package ic.swarm.app.service


import ic.base.strings.ext.isBlank
import ic.base.strings.ext.split
import ic.base.throwables.AlreadyExistsException
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.MessageException
import ic.base.throwables.UnableToParseException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.storage.fs.File
import ic.storage.fs.local.ext.createOrThrowAlreadyExists
import ic.stream.output.ext.write

import ic.cmd.Command
import ic.cmd.Console

import ic.swarm.Swarm
import ic.swarm.access.AccessKey
import ic.swarm.ext.openInput
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.link.ext.fromUrlOrThrowUnableToParse
import ic.swarm.modes.ReadMode


class DownloadFileCommand (

	private val client : Swarm

) : Command() {


	override val name        get() = "download"
	override val syntax      get() = "download <file url> <target file path>"
	override val description get() = "Upload file to swarm"


	@Throws(InvalidSyntaxException::class, MessageException::class)
	override fun implementRun (console: Console, args: String) {

		if (args.isBlank) throw InvalidSyntaxException

		val splittedArgs = args.split(separator = ' ')

		if (splittedArgs.length != 2L) throw InvalidSyntaxException

		val urlString = splittedArgs[0]

		val filePath = splittedArgs[1]

		val swarmLink = (
			try {
				SwarmLink.fromUrlOrThrowUnableToParse(urlString)
			} catch (_: UnableToParseException) {
				throw MessageException(
					message = "Can't parse swarm link: $urlString"
				)
			}
		)

		val input = client.openInput(
			streamAddress = swarmLink.streamAddress,
			readMode = ReadMode.Default,
			accessKey = AccessKey.None
		)

		val file = try {
			File.createOrThrowAlreadyExists(filePath)
		} catch (t: AlreadyExistsException) {
			throw MessageException(
				message = "File $filePath already exists"
			)
		}

		file.openOutput().runAndCloseOrCancel {
			write(input)
		}

	}


}