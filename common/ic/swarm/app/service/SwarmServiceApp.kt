package ic.swarm.app.service


import ic.app.tier.Tier
import ic.app.tier.tierString
import ic.service.ServiceApp
import ic.struct.list.List
import ic.struct.value.ext.getValue
import ic.struct.value.lazy.Lazy

import ic.swarm.Swarm


class SwarmServiceApp : ServiceApp() {


	override fun implementGetDefaultTier() : Tier {
		if (tierString == "main") {
			return Tier.Production
		} else {
			return super.implementGetDefaultTier()
		}
	}


	private val swarm by Lazy {
		Swarm(
			toUseUpnp = (
				when (tierString) {
					"nonserver" -> true
					else        -> false
				}
			),
			supportsBlocks = tierString != "main",
			reuploadImportantData = {}
		)
	}


	override fun initService (args: String) = swarm


	override fun initAdditionalCommands() = List(
		UploadFileCommand   (swarm = swarm),
		DownloadFileCommand (client = swarm)
	)


}