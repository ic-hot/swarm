package ic.swarm.app.service


import ic.base.strings.ext.isBlank
import ic.base.strings.ext.split
import ic.base.throwables.InvalidSyntaxException
import ic.base.throwables.IoException
import ic.base.throwables.MessageException
import ic.base.throwables.NotExistsException
import ic.ifaces.lifecycle.closeable.ext.runAndCloseOrCancel
import ic.storage.fs.File
import ic.storage.fs.local.ext.getExistingOrThrowNotExists
import ic.stream.output.ext.write
import ic.struct.list.ext.length.lastOrNull

import ic.cmd.Command
import ic.cmd.Console

import ic.swarm.Swarm
import ic.swarm.fs.link.SwarmLink
import ic.swarm.fs.type.ContentType
import ic.swarm.modes.WriteMode
import ic.swarm.stream.output.SwarmByteOutput


internal class UploadFileCommand (

	private val swarm : Swarm

) : Command() {


	override val name get() = "upload"
	override val syntax get() = "upload <file path>"
	override val description get() = "Upload file to swarm"


	@Throws(InvalidSyntaxException::class, MessageException::class)
	override fun implementRun (console: Console, args: String) {

		if (args.isBlank) throw InvalidSyntaxException

		val filePath = args

		val file = try {
			File.getExistingOrThrowNotExists(filePath)
		} catch (t: NotExistsException) {
			throw MessageException(
				"File $filePath not exists"
			)
		}

		val output = SwarmByteOutput(
			swarm = swarm,
			writeMode = WriteMode.Default
		)

		try {
			output.runAndCloseOrCancel {
				write(file.openInput())
			}
		} catch (_: IoException) {
			console.writeLine(
				"Unable to connect to at least one node. Check your network connection."
			)
			return
		}

		val fileName = filePath.split('/').lastOrNull

		console.writeLine("Link to your file:")
		console.writeNewLine()
		console.writeLine(
			SwarmLink(
				streamAddress = output.streamAddress,
				access = ic.swarm.access.Access.Everyone,
				contentType = ContentType.File.Raw,
				name = fileName
			).toString()
		)

	}


}